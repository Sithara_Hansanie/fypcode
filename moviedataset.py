import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O 
from collections import Counter

from sklearn.impute import SimpleImputer # used for handling missing data
from sklearn.preprocessing import LabelEncoder, OneHotEncoder# used for encoding categorical data
from sklearn.model_selection import train_test_split #used for splitting training and testing data
from sklearn.preprocessing import StandardScaler # used for feature scaling

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity

moviedata = pd.read_csv('IMDBdata_MainData.csv')


# Splitting the attributes into independent and dependent attributes
X = moviedata.iloc[:, :-1].values # attributes to determine dependent variable / Class
Y = moviedata.iloc[:, -1].values # dependent variable / Class
moviedata.info()

# handling the missing data and replace missing values with nan from numpy and replace with mean of all the other values
imputer = SimpleImputer(missing_values=np.nan, strategy='mean') 
imputer = imputer.fit(X[:, 1:])
X[:, 1:] = imputer.transform(X[:, 1:])

# encode categorical data
labelencoder_X = LabelEncoder()
X[:, 0] = labelencoder_X.fit_transform(X[:, 0])
onehotencoder = OneHotEncoder(categorical_features=[0])
X = onehotencoder.fit_transform(X).toarray()
labelencoder_Y = LabelEncoder()
Y = labelencoder_Y.fit_transform(Y)

# splitting the dataset into training set and test set
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.3, random_state=0)

# feature scaling

sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)


moviedata.head()


#  splitting the individual grouped genre into individual genre
new_dataset = pd.DataFrame(columns = ['Title','Year','Genre','Actors','Plot','Language','imdbRating','imdbID','BoxOffice'])


for i in range(len(moviedata['Genre'])):  # GO over the Genre
    for word in moviedata['Genre'][i].split(","): # split the Genre
        new_dataset = new_dataset.append({'Title':moviedata['Title'][i],'Year':moviedata['Year'][i],'Genre':word,'Actors':moviedata['Actors'][i],'Plot':moviedata['Plot'][i],'Language':moviedata['Language'][i],'imdbRating':moviedata['imdbRating'][i],'imdbID':moviedata['imdbID'][i],'BoxOffice':moviedata['BoxOffice'][i]}, ignore_index = 1)
# Checking the new data created
new_dataset.info()
new_dataset

Genre_count = Counter(new_dataset['Genre'])
Genre_count

new_dataset.head()
#Select Action Movies
is_Action = new_dataset['Genre']== 'Action'
print(is_Action.head())
actionMovies = new_dataset[is_Action]
print(actionMovies.shape)
actionMovies.head()

#Filter Language
Language_count =Counter(actionMovies['Language'])
Language_count
Year_count =Counter(actionMovies['Year'])
Year_count
actionMovies['BoxOffice'] = actionMovies['BoxOffice'].str.replace('$','')#Remove $ from Box office revenue
actionMovies['BoxOffice'] = actionMovies['BoxOffice'].str.replace(',','')
actionMovies['Plot'] = actionMovies['Plot'].str.replace('""',' ')


#select English movies
actionMovie = pd.DataFrame(actionMovies.Language.str.split(',',n= 8,expand =True))
actionMovies["Lang0"]=actionMovie[0]
actionMovies["Lang1"]=actionMovie[1]
actionMovies["Lang2"]=actionMovie[2]
actionMovies["Lang3"]=actionMovie[3]
actionMovies["Lang4"]=actionMovie[4]
actionMovies["Lang5"]=actionMovie[5]
actionMovies["Lang6"]=actionMovie[6]
actionMovies["Lang7"]=actionMovie[7]
actionMovies["Lang8"]=actionMovie[8]

actionMovies.drop(columns=["Language"],inplace=True)
actionMovies
actionMovies.drop(columns=["Lang1","Lang2","Lang3","Lang4","Lang5","Lang6","Lang7","Lang8"],inplace=True)

actionMovies.rename(columns={"Lang0":"Language"},inplace=True)
#select English movies
isEnglish = actionMovies['Language'] == 'English'
actionMovies= actionMovies[isEnglish]
actionMovies.head()

actionMovies.sort_values(by=['Year'], inplace=True)

# handling the missing data and replace missing values with nan from numpy and replace with mean of all the other values

count_vectorizer = CountVectorizer(stop_words='english')
count_vectorizer = CountVectorizer()
combine_features=['combine_features']
X_train_counts = count_vectorizer.fit_transform(combine_features)

doc_term_matrix =X_train_counts.todense()
act =pd.DataFrame(doc_term_matrix,count_vectorizer.get_feature_names(),index=[''])

#X_train_counts = count_vect.fit_transform(corpus)
 
#pd.DataFrame(X_train_counts.toarray(),columns=count_vect.get_feature_names(),index=['Document 0','Document 1'])

act
results = new_dataset.dtypes
print(results)

features = ['Actors','Plot']

def combine_features(row):
    return row['Actors']+""+row['Plot']
actionMovies['combine_features'] = actionMovies['combine_features'].str.replace(',','')

actionMovies['ID']=actionMovies['imdbID']
actionMovies
actionMovies['ID'] = actionMovies['ID'].str.replace('tt','')#Remove $ from Box office revenue
print(actionMovies.dtypes)
actionMovies['ID'] = actionMovies['ID'].astype(int)


for feature in features:
    actionMovies[feature]=actionMovies[feature].fillna('')
actionMovies["combine_features"]=actionMovies.apply(combine_features,axis=1)

actionMovies['index1'] = actionMovies.index


cv =CountVectorizer()
count_matrix =cv.fit_transform(actionMovies["combine_features"])

cosine_sim =cosine_similarity(count_matrix)
print(cosine_sim)

#find a method under this section
def get_title_from_index(ID):
    return actionMovies[actionMovies.ID == ID]["Title"].values[0]

def get_index_from_title(Title):
    return actionMovies[actionMovies.Title == Title]["ID"].values[0]

movie_user_likes ="The General"
movie_index =get_index_from_title(movie_user_likes)
similar_movies =  list(enumerate(cosine_sim[movie_index]))

sorted_similar_movies = sorted(similar_movies,key=lambda x:x[1],reverse=True)[1:]

i=0
print("Top 5 movies to "+movie_user_likes+" are:\n")
for element in sorted_similar_movies:
    print(get_title_from_index(element[0]))
    i=i+1
    if i>5:
        break




export_csv = actionMovies.to_csv (r'F:\L4S1\project\fypcode\export_dataframe.csv', index = None, header=True) #Don't forget to add '.csv' at the end of the path